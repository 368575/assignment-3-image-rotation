#include "bmp.h"
#include "rotate.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv )
{
    if (argc != 4) {
        fprintf(stderr, "Usage: %s <source-image> <transformed-image> <angle>\n", argv[0]);
        return EXIT_FAILURE;
    }

    char* end;
    long angle = strtol(argv[3], &end, 10);
    if (errno == EINVAL || errno == ERANGE || *end) {
        fprintf(stderr, "Invalid angle value: %s\n", argv[3]);
        return EXIT_FAILURE;
    }

    if (angle < -270 || angle > 270 || angle % 90) {
        fprintf(stderr, "Angle must be 0, 90, -90, 180, -180, 270 or -270\n");
        return EXIT_FAILURE;
    }

    FILE* in = fopen(argv[1], "rb");
    if (!in) {
        perror(argv[1]);
        return EXIT_FAILURE;
    }

    struct image source = {0};
    if (from_bmp(in, &source) != READ_OK) {
        fprintf(stderr, "Error reading bmp file %s\n", argv[1]);
        fclose(in);
        return EXIT_FAILURE;
    }

    fclose(in);

    struct image rotated = rotate(source, angle);
    image_free(&source);

    FILE* out = fopen(argv[2], "wb");
    if (!out) {
        perror(argv[2]);
        image_free(&rotated);
        return EXIT_FAILURE;
    }

    enum write_status status = to_bmp(out, &rotated);
    image_free(&rotated);
    fclose(out);
    if (status != WRITE_OK) {
        fprintf(stderr, "Error writing bmp file %s\n", argv[2]);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
