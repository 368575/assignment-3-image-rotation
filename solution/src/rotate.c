#include "image.h"
#include "rotate.h"
#include <string.h>

static struct image rotate_90(struct image source) {
    struct image result = {0};
    if (source.data && image_init(&result, source.height, source.width)) {
        for (uint32_t i = 0; i < result.height; i++) {
            for (uint32_t j = 0; j < result.width; j++) {
                result.data[result.width * i + j] = source.data[source.width * (j + 1) - 1 - i];
            }
        }
    }
    return result;
}

struct image rotate(struct image source, long angle)
{
    struct image result = {0};

    image_init(&result, source.width, source.height);
    // clang-tidy не пропускает memcpy, а memcpy_s не везде доступна.
    for (size_t i = 0; i < source.width * source.height; ++i) {
        result.data[i] = source.data[i];
    }

    angle = angle < 0 ? angle + 360 : angle;
    long rotations = angle / 90;
    for (long i = 0; i < rotations; ++i) {
        struct image tmp = rotate_90(result);
        image_free(&result);
        result = tmp;
    }
    return result;
}
