#include "bmp.h"

#define BF_TYPE 0x4D42
#define BF_RESERVED 0
#define B_OFF_BITS 54
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0
#define BI_PIXELS_PER_METER 0
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static uint32_t get_padding(uint32_t width) {
    return (4 - (width * 3) % 4) % 4;
}


enum read_status from_bmp(FILE* in, struct image* image)
{
    struct bmp_header header = {0};

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    if (header.bfType != BF_TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    if (header.biBitCount != BI_BIT_COUNT) {
        return READ_INVALID_BITS;
    }

    struct image result_image = {0};
    uint32_t width = header.biWidth;
    uint32_t height = header.biHeight;

    if (!image_init(&result_image, width, height)) {
        return READ_MEM_ERROR;
    }

    uint32_t padding = get_padding(width);
    for (uint32_t i = 0; i < height; i++) {
        if (fread(result_image.data + i * width, sizeof(struct pixel), width, in) != width) {
            image_free(&result_image);
            return READ_DATA_ERROR;
        }
        if (fseek(in, (long)padding, SEEK_CUR) == -1) {
            image_free(&result_image);
            return READ_DATA_ERROR;
        }
    }
    *image = result_image;
    return READ_OK;
}

enum write_status to_bmp(FILE* out, const struct image* image)
{
    struct bmp_header bmp_header = {0};
    uint32_t width = image->width;
    uint32_t height = image->height;
    uint32_t padding = get_padding(width);

    bmp_header.bfType = BF_TYPE;
    bmp_header.biSizeImage = (sizeof(struct pixel) * width + padding) * height;
    bmp_header.bfileSize = bmp_header.biSizeImage + sizeof(struct bmp_header);
    bmp_header.bfReserved = BF_RESERVED;
    bmp_header.bOffBits = B_OFF_BITS;
    bmp_header.biSize = BI_SIZE;
    bmp_header.biWidth = width;
    bmp_header.biHeight = height;
    bmp_header.biPlanes = BI_PLANES;
    bmp_header.biBitCount = BI_BIT_COUNT;
    bmp_header.biCompression = BI_COMPRESSION;
    bmp_header.biXPelsPerMeter = BI_PIXELS_PER_METER;
    bmp_header.biYPelsPerMeter = BI_PIXELS_PER_METER;
    bmp_header.biClrUsed = BI_CLR_USED;
    bmp_header.biClrImportant = BI_CLR_IMPORTANT;

    if (fwrite(&bmp_header, sizeof(bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    uint32_t zero_pad = 0;
    for (uint32_t i = 0; i < height; i++) {
        if (fwrite(image->data + i * width, sizeof(struct pixel), width, out) != width ||
            fwrite(&zero_pad, padding, 1, out) != 1) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
