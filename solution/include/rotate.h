#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include "image.h"

struct image rotate(struct image source, long angle);

#endif //IMAGE_TRANSFORMER_ROTATE_H
