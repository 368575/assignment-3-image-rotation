#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "image.h"
#include <stdint.h>
#include <stdio.h>

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_MEM_ERROR,
    READ_DATA_ERROR
};

enum read_status from_bmp(FILE* in, struct image* image);

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp(FILE* out, const struct image* image);

#endif //IMAGE_TRANSFORMER_BMP_H
